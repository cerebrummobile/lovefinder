package com.lovefinder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

import adapters.DetailProfileImageAdapter;
import customcontrols.CustomButton;
import customcontrols.CustomEditText;
import customcontrols.CustomWhiteTextView;
import utilities.CommonUtils;
import utilities.HttpUtils;
import utilities.PreferenceHandler;
import webservices.InstanceFactory;
import webservices.detailprofile.DetailProfileRequest;
import webservices.detailprofile.DetailProfileResponse;
import webservices.detailprofile.DetailProfileService;
import webservices.detailprofile.DetailProfileTaskCompleteListener;
import webservices.detailprofile.Userdetails;
import webservices.likedislike.LikeDislikeRequest;
import webservices.likedislike.LikeDislikeResponse;
import webservices.likedislike.LikeDislikeService;
import webservices.likedislike.LikeDislikeTaskCompleteListener;

/**
 * Created by navdeep on 17/03/16.
 */
public class DetailedProfileActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ViewPager mPager;
    private CustomEditText edittAboutYou;
    private LinearLayout matchButtonLL, likeDislikeButtonLL, mainLinearLayout;
    private final Context context = DetailedProfileActivity.this;
    private int imageCount;
    private DetailProfileImageAdapter mPagerAdapter;
    private final ArrayList<String> imagesList = new ArrayList<>();
    private String userID, profileID, openVia;
    private ImageView previousIV, nextIV;
    private AdView mAdView;
    private Toolbar mToolBar;
    private boolean showBadge;

    private CustomWhiteTextView userNameAgeTV, genderTV, cityTV, countryTV, interestedInTV, relationshipStatusTV, lookingForTV, heightTV, smokeTV,
            alcoholTV, smokeMarijuanaTV, otherDrugsTV, educationTV, childrenTV, imageCountTV;

    private CustomWhiteTextView genderTitleTV, cityTitleTV, countryTitleTV, interestedInTitleTV, relationshipStatusTitleTV, lookingForTitleTV, heightTitleTV, smokeTitleTV,
            alcoholTitleTV, smokeMarijuanaTitleTV, otherDrugsTitleTV, educationTitleTV, childrenTitleTV;

    private LinearLayout genderLL, cityLL, countryLL, interestedInLL, relationshipStatusLL, lookingForLL, heightLL, smokeLL,
            alcoholLL, smokeMarijuanaLL, otherDrugsLL, educationLL, childrenLL;

    private String userFirstNameStr, userLastNameStr, userAgeStr, userNameAgeStr, genderStr, cityStr, countryStr, interestedInStr, relationshipStatusStr, lookingForStr,
            heightStr, smokeStr, alcoholStr, smokeMarijuanaStr, otherDrugsStr, educationStr, childrenStr, aboutYouStr, latitudeStr, longitudeStr,
            maxDistanceStr, strProfileImageUrl, mProfileImageUrl;
    private String subStringLastName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_profile);

        initializeView();
        initAdMob();

        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getProfileData();
        updateUI();
    }


    @Override
    public void initializeView() {

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        CustomWhiteTextView mToolbar_title = (CustomWhiteTextView) mToolBar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolBar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar_title.setText(getResources().getString(R.string.detailed_profile));

        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        mPager = (ViewPager) findViewById(R.id.imageViewpager);
        previousIV = (ImageView) findViewById(R.id.imgPrevious);
        nextIV = (ImageView) findViewById(R.id.imgNext);
        userNameAgeTV = (CustomWhiteTextView) findViewById(R.id.userInfoTV);

        genderTV = (CustomWhiteTextView) findViewById(R.id.genderTV);
        cityTV = (CustomWhiteTextView) findViewById(R.id.cityTV);
        countryTV = (CustomWhiteTextView) findViewById(R.id.countryTV);
        interestedInTV = (CustomWhiteTextView) findViewById(R.id.interestedInTV);
        relationshipStatusTV = (CustomWhiteTextView) findViewById(R.id.relationshipTV);
        lookingForTV = (CustomWhiteTextView) findViewById(R.id.lookingForTV);
        heightTV = (CustomWhiteTextView) findViewById(R.id.heightTV);
        smokeTV = (CustomWhiteTextView) findViewById(R.id.smokeTV);
        alcoholTV = (CustomWhiteTextView) findViewById(R.id.alcoholTV);
        smokeMarijuanaTV = (CustomWhiteTextView) findViewById(R.id.smokeMarijuanaTV);
        otherDrugsTV = (CustomWhiteTextView) findViewById(R.id.otherDrugsTV);
        educationTV = (CustomWhiteTextView) findViewById(R.id.educationTV);
        childrenTV = (CustomWhiteTextView) findViewById(R.id.childrenTV);

        genderTitleTV = (CustomWhiteTextView) findViewById(R.id.genderTitleTV);
        cityTitleTV = (CustomWhiteTextView) findViewById(R.id.cityTitleTV);
        countryTitleTV = (CustomWhiteTextView) findViewById(R.id.countryTitleTV);
        interestedInTitleTV = (CustomWhiteTextView) findViewById(R.id.interestedInTitleTV);
        relationshipStatusTitleTV = (CustomWhiteTextView) findViewById(R.id.relationshipTitleTV);
        lookingForTitleTV = (CustomWhiteTextView) findViewById(R.id.lookingForTitleTV);
        heightTitleTV = (CustomWhiteTextView) findViewById(R.id.heightTitleTV);
        smokeTitleTV = (CustomWhiteTextView) findViewById(R.id.smokeTitleTV);
        alcoholTitleTV = (CustomWhiteTextView) findViewById(R.id.alcoholTitleTV);
        smokeMarijuanaTitleTV = (CustomWhiteTextView) findViewById(R.id.smokeMarijuanaTitleTV);
        otherDrugsTitleTV = (CustomWhiteTextView) findViewById(R.id.otherDrugsTitleTV);
        educationTitleTV = (CustomWhiteTextView) findViewById(R.id.educationTitleTV);
        childrenTitleTV = (CustomWhiteTextView) findViewById(R.id.childrenTitleTV);

        genderLL = (LinearLayout) findViewById(R.id.genderLL);
        cityLL = (LinearLayout) findViewById(R.id.cityLL);
        countryLL = (LinearLayout) findViewById(R.id.countryLL);
        interestedInLL = (LinearLayout) findViewById(R.id.interestedInLL);
        relationshipStatusLL = (LinearLayout) findViewById(R.id.relationshipLL);
        lookingForLL = (LinearLayout) findViewById(R.id.lookingForLL);
        heightLL = (LinearLayout) findViewById(R.id.heightLL);
        smokeLL = (LinearLayout) findViewById(R.id.smokeLL);
        alcoholLL = (LinearLayout) findViewById(R.id.alcoholLL);
        smokeMarijuanaLL = (LinearLayout) findViewById(R.id.smokeMarijuanaLL);
        otherDrugsLL = (LinearLayout) findViewById(R.id.otherDrugsLL);
        educationLL = (LinearLayout) findViewById(R.id.educationLL);
        childrenLL = (LinearLayout) findViewById(R.id.childrenLL);

        edittAboutYou = (CustomEditText) findViewById(R.id.edittAboutYou);
        ImageView disLikeIV = (ImageView) findViewById(R.id.no_imageView);
        ImageView likeIV = (ImageView) findViewById(R.id.yes_imageView);
        CustomButton messageBT = (CustomButton) findViewById(R.id.messageBT);
        CustomButton meetNowBT = (CustomButton) findViewById(R.id.meetNowBT);
        matchButtonLL = (LinearLayout) findViewById(R.id.matchButtonLL);
        likeDislikeButtonLL = (LinearLayout) findViewById(R.id.buttonLikeDislikeLL);
        imageCountTV = (CustomWhiteTextView) findViewById(R.id.countTV);

        CommonUtils.enableVerticalScroll(edittAboutYou, R.id.edittAboutYou);

        meetNowBT.setOnClickListener(this);
        messageBT.setOnClickListener(this);
        likeIV.setOnClickListener(this);
        disLikeIV.setOnClickListener(this);
        nextIV.setOnClickListener(this);
        previousIV.setOnClickListener(this);
        mPager.addOnPageChangeListener(this);

        mPagerAdapter = new DetailProfileImageAdapter(this, imagesList);
        mPager.setPageMargin(16);
        mPager.setPageMarginDrawable(android.R.color.transparent);
        mPager.setAdapter(mPagerAdapter);

        mainLinearLayout.setVisibility(View.GONE);
        previousIV.setVisibility(View.GONE);


        profileID = getIntent().getStringExtra("ID");
        openVia = getIntent().getStringExtra("PreviousScreen");
        userID = PreferenceHandler.readString(DetailedProfileActivity.this, PreferenceHandler.USER_ID, "");
    }


    /**
     * Adding Banner AdMob
     */
    private void initAdMob() {
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest mAdRequest = new AdRequest.Builder().build();
        mAdView.loadAd(mAdRequest);
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
        showBadge = PreferenceHandler.readBoolean(DetailedProfileActivity.this, PreferenceHandler.SHOW_BADGE, false);
        invalidateOptionsMenu();
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }


    /**
     * Method to get user's profile data from service
     */
    private void getProfileData() {
        if (CommonUtils.isNetworkAvailable(context)) {

            //set Profile Id and User ID to the service request
            final DetailProfileRequest request = new DetailProfileRequest();
            request.setUId(userID);
            request.setProfileId(profileID);

            //Call web service and handle its listener
            DetailProfileService detailProfileService = InstanceFactory.getInstance(DetailProfileService.class);
            if (detailProfileService != null) {
                detailProfileService.detailProfile(context, getResources().getString(R.string.get_profile_detail), HttpUtils.getJsonInput(request), new DetailProfileTaskCompleteListener<DetailProfileResponse>() {

                    @Override
                    public void onSuccess(List<DetailProfileResponse> result) {

                        if (result != null && result.size() > 0) {

                            ArrayList<Userdetails> userDetails = new ArrayList<>();
                            userDetails.add(result.get(0).getUserdetails().get(0));


                            String profileInfoStatus = result.get(0).getPrivacyStatus().getProfileInfo();
                            boolean showCompleteInfo = checkProfilePrivacy(profileInfoStatus);

                            userFirstNameStr = CommonUtils.checkData(userDetails.get(0).getFirstName());
                            userLastNameStr = CommonUtils.checkData(userDetails.get(0).getLastName());
                            subStringLastName = userLastNameStr.substring(0, 1).toUpperCase();

                            userAgeStr = CommonUtils.checkData(userDetails.get(0).getAge());
                            userNameAgeStr = userFirstNameStr + " " + subStringLastName +
                                    ",  " + userAgeStr + getResources().getString(R.string.yrs);
                            cityStr = CommonUtils.checkData(userDetails.get(0).getCityId());
                            countryStr = CommonUtils.checkData(userDetails.get(0).getCountryId());
                            genderStr = CommonUtils.checkData(userDetails.get(0).getSex());
                            latitudeStr = CommonUtils.setDefaultValues(userDetails.get(0).getLatitude(), "latlong");
                            longitudeStr = CommonUtils.setDefaultValues(userDetails.get(0).getLongitude(), "latlong");
                            maxDistanceStr = CommonUtils.setDefaultValues(userDetails.get(0).getMax_distance(), "distanceMax");

                            if (showCompleteInfo) {
                                interestedInStr = CommonUtils.checkData(userDetails.get(0).getLooking_type());
                                relationshipStatusStr = CommonUtils.checkData(userDetails.get(0).getRelationStatus());
                                lookingForStr = CommonUtils.checkData(userDetails.get(0).getLooking_relation());
                                lookingForStr = lookingForStr.replace(", ", ",");
                                lookingForStr = lookingForStr.replace(",", ", ");
                                heightStr = CommonUtils.checkData(userDetails.get(0).getHeight());
                                smokeStr = CommonUtils.checkData(userDetails.get(0).getSmoke());
                                alcoholStr = CommonUtils.checkData(userDetails.get(0).getAlcohol());
                                smokeMarijuanaStr = CommonUtils.checkData(userDetails.get(0).getSmoke_marijuana());
                                otherDrugsStr = CommonUtils.checkData(userDetails.get(0).getOther_drugs());
                                educationStr = CommonUtils.checkData(userDetails.get(0).getEducation());
                                childrenStr = CommonUtils.checkData(userDetails.get(0).getChildren());
                                aboutYouStr = CommonUtils.checkData(userDetails.get(0).getAbout_you());


                                CommonUtils.myLog("UserDetail", heightStr + "  " + showCompleteInfo);
                            }

                            String showGalleryStatus = CommonUtils.checkData(result.get(0).getPrivacyStatus().getShowGallery());
                            boolean showAllImages = checkProfilePrivacy(showGalleryStatus);
                            mProfileImageUrl = CommonUtils.checkData(userDetails.get(0).getProfileimage());

                            strProfileImageUrl = CommonUtils.checkData(userDetails.get(0).getProfileimage());

                            if (showAllImages && !(userDetails.get(0).getUserImage().isEmpty())) {
                                imageCount = userDetails.get(0).getUserImage().size();
                                for (int i = 0; i < imageCount; i++) {

                                    strProfileImageUrl = strProfileImageUrl.replace(getResources().getString(R.string.ftp_folder_path), "").trim();
                                    strProfileImageUrl = strProfileImageUrl.replace(".png", "");
                                    strProfileImageUrl = strProfileImageUrl.replace(".jpeg", "");
                                    strProfileImageUrl = strProfileImageUrl.replace(".jpg", "");
                                    String galleryImageName = userDetails.get(0).getUserImage().get(i).getName();

                                    if (galleryImageName.contains(strProfileImageUrl)) {
                                        imagesList.add(0, userDetails.get(0).getUserImage().get(i).getGallery_Image());
                                        CommonUtils.myLog("ProfileImage", "ProfileImage found at " + i + " position.");

                                    } else {
                                        imagesList.add(userDetails.get(0).getUserImage().get(i).getGallery_Image());
                                    }
                                }

                            } else {

                                imagesList.add(strProfileImageUrl);
                            }
                            if (imagesList.size() == 1) {
                                updateButtonUI(false, previousIV);
                                updateButtonUI(false, nextIV);
                            }
                            imageCount = imagesList.size();
                            mPagerAdapter.notifyDataSetChanged();
                            mainLinearLayout.setVisibility(View.VISIBLE);
                            setData(showCompleteInfo);

                        } else {
                            CommonUtils.showLongToast(context, getResources().getString(R.string.server_error));
                            DetailedProfileActivity.this.finish();
                        }

                    }

                    @Override
                    public void onError(String error) {
                        CommonUtils.showLongToast(context, error);
                        DetailedProfileActivity.this.finish();
                    }

                    @Override
                    public void onRetry(String error) {
                        CommonUtils.showLongToast(context, error);
                        DetailedProfileActivity.this.finish();

                    }
                }, true);
            } else {
                CommonUtils.showLongToast(context, getResources().getString(R.string.server_error));
                DetailedProfileActivity.this.finish();
            }

        } else {
            CommonUtils.showLongToast(DetailedProfileActivity.this, getResources().getString(R.string.no_internet));
            DetailedProfileActivity.this.finish();
        }

    }


    /**
     * Method to update UI and set View with data
     *
     * @param showCompleteInfo TRUE if user has access to view the full information; FALSE otherwise
     */
    private void setData(boolean showCompleteInfo) {
        userNameAgeTV.setText(userNameAgeStr);
        imageCountTV.setText(imageCount + "");

        setDataToViews(genderStr, genderTV, genderLL, genderTitleTV, getResources().getString(R.string.gender));
        setDataToViews(cityStr, cityTV, cityLL, cityTitleTV, getResources().getString(R.string.city));
        setDataToViews(countryStr, countryTV, countryLL, countryTitleTV, getResources().getString(R.string.country));

        if (showCompleteInfo) {
            //Check for interested in Info
            if (interestedInStr.contains("Everyone")) {
                interestedInStr = "Men, Women";
                CommonUtils.myLog("UserDetail", interestedInStr);
            }

            //Check for about you
            if (aboutYouStr == null || aboutYouStr.equals("")) {
                edittAboutYou.setVisibility(View.GONE);
            } else {
                edittAboutYou.setText(aboutYouStr);
            }

            //Check For height
            if (heightStr == null || heightStr.equals(".")) {
                heightLL.setVisibility(View.GONE);
            } else {
                heightStr = checkForHeight(heightStr);
            }

            if (heightStr.equals("00.00")) {
                heightLL.setVisibility(View.GONE);
            }

            setDataToViews(heightStr, heightTV, heightLL, heightTitleTV, getResources().getString(R.string.height));
            setDataToViews(interestedInStr, interestedInTV, interestedInLL, interestedInTitleTV, getResources().getString(R.string.interested_in));
            setDataToViews(relationshipStatusStr, relationshipStatusTV, relationshipStatusLL, relationshipStatusTitleTV, getResources().getString(R.string.relationshipStatus));
            setDataToViews(lookingForStr, lookingForTV, lookingForLL, lookingForTitleTV, getResources().getString(R.string.lookingFor));
            setDataToViews(smokeStr, smokeTV, smokeLL, smokeTitleTV, getResources().getString(R.string.smoke));
            setDataToViews(alcoholStr, alcoholTV, alcoholLL, alcoholTitleTV, getResources().getString(R.string.alcohol));
            setDataToViews(smokeMarijuanaStr, smokeMarijuanaTV, smokeMarijuanaLL, smokeMarijuanaTitleTV, getResources().getString(R.string.smoke_marijuana));
            setDataToViews(otherDrugsStr, otherDrugsTV, otherDrugsLL, otherDrugsTitleTV, getResources().getString(R.string.other_drugs));
            setDataToViews(educationStr, educationTV, educationLL, educationTitleTV, getResources().getString(R.string.education));
            setDataToViews(childrenStr, childrenTV, childrenLL, childrenTitleTV, getResources().getString(R.string.children));

        } else {
            interestedInLL.setVisibility(View.GONE);
            relationshipStatusLL.setVisibility(View.GONE);
            lookingForLL.setVisibility(View.GONE);
            heightLL.setVisibility(View.GONE);
            smokeLL.setVisibility(View.GONE);
            alcoholLL.setVisibility(View.GONE);
            smokeMarijuanaLL.setVisibility(View.GONE);
            otherDrugsLL.setVisibility(View.GONE);
            educationLL.setVisibility(View.GONE);
            childrenLL.setVisibility(View.GONE);
            edittAboutYou.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.no_imageView:
                likeDislike("0");
                break;

            case R.id.yes_imageView:
                likeDislike("1");
                break;

            case R.id.messageBT:
                if (CommonUtils.isNetworkAvailable(context)) {
                    Intent intent = new Intent(DetailedProfileActivity.this, ComposeActivityDialog.class);
                    intent.putExtra("ReceiverID", profileID);
                    intent.putExtra("ReceiverName", userFirstNameStr + " " + subStringLastName);
                    startActivity(intent);
                } else {
                    CommonUtils.showSmallToast(context, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.meetNowBT:
                if (CommonUtils.isNetworkAvailable(context)) {
                    startMeetNowPlacesActivity();
                } else {
                    CommonUtils.showSmallToast(context, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.imgNext:
                setNextPage(mPager.getCurrentItem() + 1);
                break;

            case R.id.imgPrevious:
                setNextPage(mPager.getCurrentItem() - 1);
                break;

            default:
                break;

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem action_notification = menu.findItem(R.id.action_notification);
        MenuItem action_unmatch = menu.findItem(R.id.action_unmatch);
        MenuItem notificationItem = menu.findItem(R.id.action_notification);
        MenuItem notificationBadgeItem = menu.findItem(R.id.action_notification_badge);

        MenuItem dealItem = menu.findItem(R.id.action_deal);
        MenuItem refreshItem = menu.findItem(R.id.action_refresh);

        refreshItem.setVisible(false);
        dealItem.setVisible(false);

        String OpenViaMatches = getResources().getString(R.string.matches);

        if (OpenViaMatches.equals(openVia)) {
            action_unmatch.setVisible(true);

        } else {
            action_notification.setVisible(false);
        }

        if (showBadge) {
            notificationBadgeItem.setIcon(CommonUtils.buildCounterDrawable(DetailedProfileActivity.this, MainActivity.notificationCount, R.drawable.ic_menu_notification));
        }

        notificationBadgeItem.setVisible(showBadge);
        notificationItem.setVisible(!showBadge);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification:
            case R.id.action_notification_badge:
                startActivity(new Intent(DetailedProfileActivity.this, NotificationsActivity.class));
                break;

            case R.id.action_unmatch:
                likeDislike("0");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method for like or dislike user profile
     */
    private void likeDislike(final String like_status) {
        if (CommonUtils.isNetworkAvailable(context)) {

            final LikeDislikeRequest request = new LikeDislikeRequest();

            request.setUId(userID);
            request.setLike_status(like_status);
            request.setTo_uid(profileID);


            LikeDislikeService likeDislikeService = InstanceFactory.getInstance(LikeDislikeService.class);
            if (likeDislikeService != null) {
                likeDislikeService.likeDislike(context, getResources().getString(R.string.like_dislike_url), HttpUtils.getJsonInput(request), new LikeDislikeTaskCompleteListener<LikeDislikeResponse>() {
                    @Override
                    public void onSuccess(List<LikeDislikeResponse> result) {
                        if (result != null && result.size() > 0) {

                            String strLikeMatchStatus = result.get(0).getLike_Match_Status();

                            MainActivity.getDealBreakerData = true;
                            MainActivity.refreshMatches = true;
                            MainActivity.refreshSearch = true;

                            if (like_status.equalsIgnoreCase("0")) {
                                CommonUtils.showLongToast(context, getResources().getString(R.string.profile_disliked));
                                PreferenceHandler.writeBoolean(context, PreferenceHandler.USER_UNMATCHED, true);
                                DetailedProfileActivity.this.finish();
                            } else {
                                CommonUtils.showLongToast(context, getResources().getString(R.string.profile_liked));
                                DetailedProfileActivity.this.finish();
                            }


                            if (strLikeMatchStatus.equalsIgnoreCase("2")) {
                                startMeetNowActivity();
                            } else {
                                //do nothing
                            }
                        } else {
                            CommonUtils.showLongToast(context, getResources().getString(R.string.server_error));

                        }
                    }

                    @Override
                    public void onError(String error) {
                        CommonUtils.showLongToast(context, error);
                    }

                    @Override
                    public void onRetry(String error) {
                        CommonUtils.showLongToast(context, error);

                    }
                }, true);
            } else {
                CommonUtils.showLongToast(context, getResources().getString(R.string.server_error));
            }
        } else {
            CommonUtils.showLongToast(DetailedProfileActivity.this, getResources().getString(R.string.no_internet));
        }

    }

    /**
     * Method to update ui visibility with the origin of detail profile
     * it may be from matches fragment and may be from search fragment
     */
    private void updateUI() {
        if (openVia.equals(getResources().getString(R.string.search))) {
            matchButtonLL.setVisibility(View.GONE);
            likeDislikeButtonLL.setVisibility(View.VISIBLE);
        } else if (openVia.equals(getResources().getString(R.string.matches))) {
            matchButtonLL.setVisibility(View.VISIBLE);
            likeDislikeButtonLL.setVisibility(View.GONE);
        }

    }

    /**
     * Method to start MeetNowScreen Activity to for selected user
     */
    private void startMeetNowActivity() {
        if (CommonUtils.isNetworkAvailable(context)) {

            CommonUtils.myLog("startMeetNowActivity", "userFirstNameStr: " + userFirstNameStr);

            Bundle bundle = new Bundle();
            bundle.putString(context.getResources().getString(R.string.id), profileID);
            bundle.putString(context.getResources().getString(R.string.f_name), userFirstNameStr);
            bundle.putString(context.getResources().getString(R.string.l_name), subStringLastName);
            bundle.putString(context.getResources().getString(R.string.age), userAgeStr);
            bundle.putString(context.getResources().getString(R.string.image), mProfileImageUrl);
            CommonUtils.myLog("", "strProfileImageUrl : -- >  " + mProfileImageUrl);
            bundle.putString(context.getResources().getString(R.string.latitude), latitudeStr);
            bundle.putString(context.getResources().getString(R.string.longitude), longitudeStr);
            bundle.putString(context.getResources().getString(R.string.max_distance), maxDistanceStr);

            Intent intent = new Intent(context, MeetNowScreenActivity.class);
            intent.putExtra(getResources().getString(R.string.bundle), bundle);
            startActivity(intent);
        } else {
            CommonUtils.showLongToast(DetailedProfileActivity.this, getResources().getString(R.string.no_internet));

        }
    }

    /**
     * Method to start meet now sreen activity
     */
    private void startMeetNowPlacesActivity() {
        if (CommonUtils.isNetworkAvailable(context)) {
            Bundle bundle = new Bundle();
            bundle.putString(getResources().getString(R.string.latitude), latitudeStr);
            bundle.putString(getResources().getString(R.string.longitude), longitudeStr);
            bundle.putString(getResources().getString(R.string.max_distance), maxDistanceStr);
            bundle.putString(getResources().getString(R.string.f_name), userFirstNameStr);
            bundle.putString(getResources().getString(R.string.id), profileID);
            bundle.putBoolean(getResources().getString(R.string.invite), false);
            Intent intent = new Intent(context, MeetNowPlacesActivity.class);
            intent.putExtra(getResources().getString(R.string.bundle), bundle);
            startActivity(intent);
        } else {
            CommonUtils.showLongToast(DetailedProfileActivity.this, getResources().getString(R.string.no_internet));

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //do nothing
    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            updateButtonUI(false, previousIV);
            updateButtonUI(true, nextIV);
        } else if (position == imagesList.size() - 1) {
            updateButtonUI(false, nextIV);
            updateButtonUI(true, previousIV);
        } else {
            updateButtonUI(true, previousIV);
            updateButtonUI(true, nextIV);
        }
    }

    /**
     * Method to update button UI
     *
     * @param enable    whether to show the imageView or not
     * @param imageView imageView
     */
    private void updateButtonUI(boolean enable, ImageView imageView) {
        if (enable) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //do nothing
    }


    /**
     * Set Next View Pager's Page
     *
     * @param position current available position
     */
    private void setNextPage(int position) {
        mPager.setCurrentItem(position);
        mPagerAdapter.notifyDataSetChanged();
    }


    /**
     * Method to check the user Privacy status for Profile Info and Gallery Images
     *
     * @param privacyStatus
     * @return boolean
     */
    private boolean checkProfilePrivacy(String privacyStatus) {

        String noOne = "0", everyOne = "1", matches = "2";
        String OpenViaMatches = getResources().getString(R.string.matches);
        return privacyStatus.equals(everyOne) || (privacyStatus.equals(matches) && openVia.equals(OpenViaMatches));
    }

    /**
     * Method to check height of profile user according to feet and inch
     *
     * @param heightStr height the current user
     * @return
     */
    private String checkForHeight(String heightStr) {
        String feet = "00", inch = "00";
        String arr[] = heightStr.split("\\.");
        int arrSize = arr.length;
        if (arrSize == 1) {
            feet = arr[0];
        } else {
            if (!(arr[0].equals(null) || arr[0].equals(""))) {
                feet = arr[0];
            }
            if (!(arr[1].equals(null) || arr[1].equals(""))) {
                inch = arr[1];
            }
        }
        if (feet == null || feet.equals("")) {
            feet = "00";
        }

        if (inch == null || inch.equals("")) {
            inch = "00";
        }
        return feet + "." + inch;
    }

    /**
     * Method to check string data and set it to view
     */
    private void setDataToViews(String value, CustomWhiteTextView view, LinearLayout linearLayout, CustomWhiteTextView titleView, String title) {
        if (value == null || value.trim().equals("")) {
            linearLayout.setVisibility(View.GONE);
        } else {
            titleView.setText(title + " - ");
            view.setText(value);
        }
    }

}
